# The Python program for quick sort
def partition(arr,a,z): 
    i = ( a-1 )          
    pivot = arr[z]    
  
    for T in range(a , z): 
  
        # If current element is smaller than the pivot 
        if   arr[T] < pivot: 
          
            # increment index of smaller element 
            i = i+1 
            arr[i],arr[T] = arr[T],arr[i] 
  
    arr[i+1],arr[z] = arr[z],arr[i+1] 
    return ( i+1 ) 
  
# The main function that implements QuickSort 
# a  --> Starting index, 
# z  --> Ending index 
  
# Function to do Quick sort 
def quickSort(arr,a,z): 
    if a < z: 
  
        # par is partitioning index, arr[par] is now at right place 
        par = partition(arr,a,z) 
  
        # Separately sort elements before partition and after partition 
        quickSort(arr, a, par-1) 
        quickSort(arr, par+1, z) 
  
print("*****************************************************")
lis = []
arr = int(input("Enter the length of an array for sorting:"))
for i in range(0,arr):
    element = int(input("Enter the %d of an array:"))
    lis.append(element)
print("***************************************************")
print("The numbers in the array is: ", lis)
print("****************************************************")
quickSort(lis,0,arr-1) 
print ("The Sorted array is:") 
for i in range(arr): 
    print ("%d" %lis[i]),
print("****************************************************")