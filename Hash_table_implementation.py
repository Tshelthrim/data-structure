def display_hash(hashTable): 
    print("This is Implementation of Hash Table")
    print("...........................................")
    for t in range(len(hashTable)): 
        print(t, end = " ") 
          
        for x in hashTable[t]: 
            print("---->", end = " ") 
            print(x, end = " ") 
        print() 
    print("..........................................")
    print("This is how Hash Table Executed!")
# Creating Hashtable as  
# a nested list. 
HashTable = [[] for _ in range(10)] 
  
# Hashing Function to return  
# key for every value. 
def Hashing(keyvalue): 
    return keyvalue % len(HashTable) 
  
  
# Insert Function to add 
# values to the hash table 
def insert(Hashtable, keyvalue, value): 
      
    hash_key = Hashing(keyvalue) 
    Hashtable[hash_key].append(value) 
  
insert(HashTable, 10, 'Thimphu') 
insert(HashTable, 25, 'Paro') 
insert(HashTable, 22, 'Punakha') 
insert(HashTable, 9, 'Haa') 
insert(HashTable, 21, 'Wangdiphodrang') 
insert(HashTable, 21, 'Monger') 
insert(HashTable, 28, 'Samdrup Jongkhar')
insert(HashTable, 6, 'Trashi Yangtse')

  
display_hash (HashTable) 