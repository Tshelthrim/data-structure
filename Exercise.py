DEBUG = False

string = 'TSHELTHRIMTENZIN'


class NodeTree(object):

    def __init__(self, left=None, right=None):
        self.left = left
        self.right = right

    def children(self):
        return (self.left, self.right)

    def nodes(self):
        return (self.left, self.right)

    def __str__(self):
        return '%s_%s' % (self.left, self.right)


def huffmanTree(node, left=True, binString=''):
    if type(node) is str:
        return {node: binString}
    (l, r) = node.children()
    x = dict()
    x.update(huffmanTree(l, True, binString + '0'))
    x.update(huffmanTree(r, False, binString + '1'))
    return x


if DEBUG:
    print('Input file:')

freq = {}
for s in string:
    if s in freq:
        freq[s] += 1
    else:
        freq[s] = 1

freq = sorted(freq.items(), key=lambda x: x[1], reverse=True)

if DEBUG:
    print(' Char | Freq ')
    for (key, s) in freq:
        print(' %4r | %d' % (key, s))

nodes = freq

while len(nodes) > 1:
    (key1, s1) = nodes[-1]
    (key2, s2) = nodes[-2]
    nodes = nodes[:-2]
    node = NodeTree(key1, key2)
    nodes.append((node, s1 + s2))

    nodes = sorted(nodes, key=lambda x: x[1], reverse=True)

if DEBUG:
    print('left: %s' % nodes[0][0].nodes()[0])
    print('right: %s' % nodes[0][0].nodes()[1])

huffmanCode = huffmanTree(nodes[0][0])

print(' Letters  | Huffman code ')
print('----------------------')
for (char, frequency) in freq:
    print(' %-4r |%12s' % (char, huffmanCode[char]))