print('******Choice to Operate:********')
print('Enter pu for push')
print('Enter po for pop')
print('Enter s to show size')
print('Enter sh to show the remaining element')
print('Enter q to quit')
class Stack:
    def __init__(self):
        self.q = Queue()

    def is_empty(self):
        return self.q.is_empty()

    def push(self, data):
        self.q.enqueue(data)

    def pop(self):
        for _ in range(self.q.get_size() - 1):
            dequeued = self.q.dequeue()
            self.q.enqueue(dequeued)
        return self.q.dequeue()

    def show(self):
        self.q.display()

    def size(self):
        print(self.q.get_size())

class Queue:
    def __init__(self):
        self.items = []
        self.size = 0

    def is_empty(self):
        return self.items == []

    def enqueue(self, data):
        self.size += 1
        self.items.append(data)

    def dequeue(self):
        self.size -= 1
        return self.items.pop(0)

    def get_size(self):
        return self.size

    def display(self):
        self.lst=[]
        for data in self.items:
            self.lst.append(data)
        print(self.lst)    
 
s = Stack()

while True:
    choice = input('\nEnter the choice to operate the program: ').split()
 
    operation = choice[0].strip().lower()
    if operation == 'pu':
        s.push(int(choice[1]))

    elif operation == 'po':
        if s.is_empty():
            print('Please push some elements to pop.')
        else:
            print('The Popped element is: ', s.pop())

    elif operation == 's':
        s.size()

    elif operation=='sh':
        if s.is_empty():
            print('No remaining elements to display.')
        else:
            s.show()
    elif operation == 'q':
        break
