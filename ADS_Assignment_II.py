 #Implementation of binary search.
array = [ 2, 3, 5, 10, 15, 20,30 ]
print('The list of an array is:', array)
s = int(input("Enter the number you want to see the position from the above array: "))

def binarySearch (array, l, r, s): 

    if r >= l: 
  
        center = l + (r - l) // 2
  
        if array[center] == s: 
            return center 
   
        elif array[center] > s: 
            return binarySearch(array, l, center-1, s) 
        else: 
            return binarySearch(array, center + 1, r, s) 
    else: 
        return -1
    
result = binarySearch(array, 0, len(array)-1, s) 
  
if result != -1: 
    print ("\n\nThe position of an element which starts from  the index 0 and the position of an element given above is,  at the index of % d" % result) 
else: 
    print ("Element is not present in an array") 